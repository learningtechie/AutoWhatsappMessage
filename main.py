""" Send whatsapp message in sceduled time """
import pywhatkit #pip install pywhatkit
import datetime

ph_num = input("Enter the phone number: ")
message = input("Enter the message: ")
hour = int(datetime.datetime.now().hour)
minute = int(datetime.datetime.now().minute)

pywhatkit.sendwhatmsg(ph_num,message,hour,minute+1)
exit